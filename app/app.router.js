dl.config(['$stateProvider', '$urlRouterProvider', 'contentfulProvider', '$locationProvider', 'environmentVariables', function($stateProvider, $urlRouterProvider, contentfulProvider, $locationProvider, environmentVariables) {

  $urlRouterProvider.otherwise('/article/3rf2j4ieZyC0IUu48Euiec');

  $locationProvider.html5Mode(true);

  $stateProvider

    .state('main', {
      abstract: true,
      views: {
        '': {
          templateUrl: 'app/shared/layouts/main-layout-view.html',
          controller: 'mainLayoutController'
        }
      }
    })

    .state('article', {
      parent: 'main',
      url: '/article/:id',
      views: {
        '': {
          templateUrl: 'app/components/article/article-view.html',
          controller: 'articleController'
        }
      }
    });

  contentfulProvider.setOptions({
    space: environmentVariables.contentfulSpaceId,
    accessToken: environmentVariables.contentfulAccessToken,
  });

}]);

dl.run(function() {

});
