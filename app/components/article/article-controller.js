"use strict";

dl.controller('articleController', ['$scope', 'contentful', '$state', 'environmentVariables', '$stateParams', 'utilityFactory', function($scope, contentful, $state, environmentVariables, $stateParams, utilityFactory) {
  $scope.article = false;
  $scope.articleId = $stateParams.id;
  $scope.articleError = false;

  var getArticle = function() {
    contentful
      .entries("content_type=clientArticle&sys.id=" + $scope.articleId + "&include=3")
      .then(function(response) {
        var articleRaw = response.data.items[0];
        if(articleRaw !== undefined) {
          fillArticleContent(articleRaw);
        } else {
          $scope.articleError = true;
        }
      });
  };

  var fillArticleContent = function(articleRaw) {
    $scope.article = {
      id: articleRaw.sys.id,
      title: articleRaw.fields.c_title,
      image: utilityFactory.getImageUrlForAStory(articleRaw),
      content: utilityFactory.getContentBodyFromMarkdown(articleRaw.fields.articleBodyClient),
    };
  };

  $scope.$on('$viewContentLoaded', function() {
    getArticle();
  });
}]);
