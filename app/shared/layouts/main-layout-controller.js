"use strict";

dl.controller('mainLayoutController', ['$scope', 'contentful', '$state', 'environmentVariables', '$rootScope', function($scope, contentful, $state, environmentVariables, $rootScope) {
  document.getElementById('mainWrap').onscroll = function() {
  	if(document.getElementById('mainWrap').scrollTop >= 50) {
  	  document.getElementById('header').className = 'sticky';
  	} else {
	    document.getElementById('header').className = '';
    }
  };
}]);
