dl.factory('utilityFactory', [function() {
  var getContentBodyFromMarkdown = function(contentBody) {
    var contentHTML = '';
    var converter = new showdown.Converter();
    contentHTML = converter.makeHtml(contentBody);
    return contentHTML;
  };

  var getImageUrlForAStory = function(aStory) {
    var imageUrl = aStory.fields !== undefined && aStory.fields.articleImageClient !== undefined && aStory.fields.articleImageClient !== null && aStory.fields.articleImageClient.fields !== undefined ? aStory.fields.articleImageClient.fields.file.url : '';
    return imageUrl !== '' && imageUrl !== false && imageUrl.substr(0, 3) !== 'http' ? 'http:' + imageUrl : imageUrl;
  };

  return {
    getContentBodyFromMarkdown: getContentBodyFromMarkdown,
    getImageUrlForAStory: getImageUrlForAStory
  };
}]);
